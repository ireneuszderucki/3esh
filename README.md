# TASK 1

Utwórz prostą usługę REST oferującą wykonywanie operacji dodawania i dzielenia 
Usługa powinna udostępniać następujące endpointy:
POST /add – przyjmuje dwa argumenty val1 i val2 jako wartość typu double i zwraca wynik będący ich sumą w formacie JSON. Np.:
{ „value”: 12.92 }

GET /div – analogicznie jak /add powyżej ale dzieli val1 przez val2 i zwraca wynik. 

Poza implementacją tych dwóch mikro usług zaimplementuj testy kontrolera.
Rezultatem pracy ma być przesłane archiwum z pełnym kodem, projekt ma być buildowany mavenem, wynikiem ma być plik test.war. 
