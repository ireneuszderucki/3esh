package com.example.demo;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class OperationRestControllerTest {
	
	private static final String JSON = "{\"val1\":-40.50,\"val2\":12.50}";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnOne() throws Exception {
		this.mockMvc.perform(get("/div?val1=1&val2=1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("1")));
	}
	
	@Test
	public void shouldReturnExceptionInfo() throws Exception {
		this.mockMvc.perform(get("/div?val1=1&val2=0")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("You cannot divide by '0'")));
	}
	
	@Test
	public void shouldReturnValidResult() throws Exception {
		this.mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(JSON))
		.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("-28.0")));
	}

}
