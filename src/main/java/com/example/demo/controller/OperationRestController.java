package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.InputData;
import com.example.demo.model.OutputResponse;
import com.example.demo.service.OperationService;

@RestController
public class OperationRestController {

	@Autowired
	private OperationService operationService;


	@PostMapping("/add")
	@ResponseBody
	public OutputResponse add(@RequestBody InputData inputData) {

		OutputResponse response = new OutputResponse(
				operationService.tryToAdd(inputData.getVal1(), inputData.getVal2()));
		return response;

	}

	@GetMapping("/div")
	@ResponseBody
	public OutputResponse divide(@RequestParam double val1, @RequestParam double val2) {
		return new OutputResponse(operationService.tryToDivide(val1, val2));
	}

}
