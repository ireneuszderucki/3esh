package com.example.demo.impl;

import com.example.demo.service.OperationService;

public class OperationServiceImpl implements OperationService {

	@Override
	public String tryToAdd(double val1, double val2) {
		return String.valueOf(val1 + val2);
	}

	@Override
	public String tryToDivide(double val1, double val2) {
		String result = null;
		try {
			if (val2 == Double.valueOf(0)) {
				throw new ArithmeticException();
			}
			result = String.valueOf(val1/val2);
		} catch (ArithmeticException e) {
			result = "You cannot divide by '0'";
		}
		return result;
	}

}
