package com.example.demo.model;

public class OutputResponse {
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public OutputResponse(String value) {
		super();
		this.value = value;
	}
	
	

}
