package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.example.demo.impl.OperationServiceImpl;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.demo.*")
public class DemoApplication {
	
	@Bean
	OperationServiceImpl operationService() {
		return new OperationServiceImpl();
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
