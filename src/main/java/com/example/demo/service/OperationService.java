package com.example.demo.service;

import org.springframework.stereotype.Component;

@Component
public interface OperationService {
	
	public String tryToAdd(double val1, double val2);
	
	public String tryToDivide(double val1, double val2);

}
